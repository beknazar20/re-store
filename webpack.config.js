const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
    mode: "development",

    module : {
        rules: [
            // {
            //     test: /\.js$/,
            //     exclude: /node_modules/,
            //     use: [
            //         {
            //             loader: 'babel-loader'
            //         }
            //     ]
            // },
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                resolve: {
                  extensions: [".js", ".jsx"]
                },
                use: {
                  loader: "babel-loader"
                }
              },
            {
                test: /\.(png|jpg|jpeg|gif|ico)$/,
                use:[{
                     loader: 'file-loader',
                     options: {
                         outputPath: 'images',
                        name: '[name]-[hash:7].[ext]'
                     }
                     }]
            },
            {
                test: /\.(ttf|otf|woff|woff2|ico)$/,
                use:[{
                     loader: 'file-loader',
                     options: {
                         outputPath: 'fonts',
                        name: '[name].[ext]'
                     }
                     }]
            },
             {
                test: /\.(css)$/,
                use:[
                    { loader: 'style-loader' },
                    { loader: 'css-loader' }
                    ]
            },
               {
                test: /\.(scss)$/,
                use:[
                    { loader: 'style-loader' },
                    { loader: 'css-loader' },
                    { loader: 'sass-loader' }
                    ]
            }
        ] 
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: 'public/index.html'
        })
    ] ,
    devServer: {
         open: true
    }
}